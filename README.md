# voicebox-audio-broadcast
This application enable user to broadcast their audio and subscribe already braodcasted audio
#how to install
step 1 : should install nodejs
```bash
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
sudo apt-get install -y nodejs

```
step 2 : clone the source code from this repository
```bash
git clone https://bitbucket.org/greenkeytech/voicebox-networkcheck.git
```
step 2 : install express and socketio
```bash
cd voicebox-networkcheck
sudo npm install express
sudo npm install socket.io
```

step 3 : run the application
```bash
node server.js
```

Now console will show the message  Server Running At port . Your application is ready
```bash
Please load the url  :   https://[your domain]:port
```
You can change the turnserver on js/RTCMultiConnection.js in static directory
```bash
iceServers.push({
            url: 'turn:turn.domain.com:80',
            credential: 'homeo',
            username: 'homeo'
        });
```
You can change the port by editing 
```bash
server.js in root directory
```

This application uses self-signed certificate so you need to allow insecure domain
for using original ssl certificate put your certificate files in 
```bash
  cert directory in root
```